To generate the Strava API library:

1. Install openapi generator: `sudo pacman -S openapi-generator`
2. Get the swagger file: `curl https://developers.strava.com/swagger/swagger.json > swagger.json`
3. Generate the API: `openapi-generator generate -i swagger.json -g rust -o api --skip-validate-spec --package-name strava`
4. Patch the broken API files:

    ```
    patch -uR api/src/apis/routes_api.rs < patches/routes_api.rs.diff
    patch -uR api/src/apis/athletes_api.rs < patches/athletes_api.rs.diff
    ```
5. Make sure the API builds: `cd api && cargo build`
