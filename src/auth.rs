use std::collections::HashMap;
use std::env;
use std::error::Error;
use std::io::{prelude::*, BufReader};
use std::net::TcpListener;
use std::time::Duration;

use oauth2::basic::BasicClient;
use oauth2::reqwest::async_http_client;
use oauth2::{
    AuthType, AuthUrl, AuthorizationCode, ClientId, ClientSecret, CsrfToken, RedirectUrl, Scope,
    TokenResponse, TokenUrl,
};
use serde::{Deserialize, Serialize};
use time::OffsetDateTime;

const SERVER_URL: &'static str = "127.0.0.1:7878";

#[derive(Deserialize, Serialize)]
pub struct Token {
    pub token: String,
    pub refresh_token: String,
    pub expires_at: OffsetDateTime,
}

/// Starts a basic webserver to get an OAuth2 code after authorization.
fn get_oauth_response() -> Result<(String, String), &'static str> {
    let listener = TcpListener::bind(SERVER_URL).unwrap();

    for stream in listener.incoming() {
        let mut stream = stream.unwrap();
        let buf_reader = BufReader::new(&mut stream);

        if let Some(Ok(first_line)) = buf_reader.lines().next() {
            let query_parameters = get_query_parameters(&first_line);
            if let (Some(code), Some(state)) =
                (query_parameters.get("code"), query_parameters.get("state"))
            {
                let response_body = "You can close this tab now.";
                let response = format!(
                    "HTTP/1.1 200 OK\r\nContent-Length: {}\r\n\r\n{}",
                    response_body.len(),
                    response_body
                );

                stream.write_all(response.as_bytes()).unwrap();
                return Ok((code.to_string(), state.to_string()));
            } else {
                return Err("No code in OAuth response!");
            }
        } else {
            return Err("Failed to read OAuth response");
        }
    }
    Err("No response from authentication.")
}

/// Converts the first line of an HTTP request into a hashmap of its query parameters, if present
fn get_query_parameters<'a>(request_line: &'a String) -> HashMap<&'a str, &'a str> {
    let path = request_line.split(" ").skip(1).next().unwrap_or("");
    path.split_once("?")
        .unwrap_or(("", ""))
        .1
        .split("&")
        .map(|s| s.split_once("=").unwrap())
        .collect()
}

/// Prompts the user to request a new authorization code
pub async fn get_new_code() -> Result<Token, Box<dyn Error>> {
    let secret = env::var("STRAVA_CLIENT_SECRET").expect("No client secret found for authorization. Set the STRAVA_CLIENT_SECRET environment variable.");

    // Create an OAuth2 client by specifying the client ID, client secret, authorization URL and
    // token URL.
    let client = BasicClient::new(
        ClientId::new("112230".to_string()),
        Some(ClientSecret::new(secret)),
        AuthUrl::new("http://www.strava.com/oauth/authorize".to_string())?,
        Some(TokenUrl::new(
            "https://www.strava.com/oauth/token".to_string(),
        )?),
    )
    // Set the URL the user will be redirected to after the authorization process.
    .set_redirect_uri(RedirectUrl::new(format!("http://{SERVER_URL}"))?)
    .set_auth_type(AuthType::RequestBody);

    // Generate the full authorization URL.
    let (auth_url, csrf_token) = client
        .authorize_url(CsrfToken::new_random)
        // Set the desired scopes.
        .add_scope(Scope::new("activity:read_all".to_string()))
        .url();

    // This is the URL you should redirect the user to, in order to trigger the authorization
    // process.
    println!("Open this page to log in to Strava: {}", auth_url);

    let time = OffsetDateTime::now_utc();

    if let Ok((code, state)) = get_oauth_response() {
        if &state != csrf_token.secret() {
            panic!("Invalid state token returned!");
        }

        if let Ok(token) = client
            .exchange_code(AuthorizationCode::new(code.trim().to_string()))
            .request_async(async_http_client)
            .await
        {
            return Ok(Token {
                token: token.access_token().secret().to_owned(),
                refresh_token: token
                    .refresh_token()
                    .map(|rt| rt.secret().to_owned())
                    .unwrap_or(String::from("")),
                expires_at: time + token.expires_in().unwrap_or(Duration::from_secs(0)),
            });
        }
    }
    Err(Box::from("Error getting response token"))
}
