use reqwest;
use serde::{Deserialize, Serialize};
use time::OffsetDateTime;

use crate::auth::Token;

const PAGE_SIZE: usize = 30;

pub struct API {
    pub token: Token,
}

#[derive(Deserialize, Serialize, Debug)]
pub struct ActivitySummary {
    name: String,
    distance: f64,
    #[serde(rename = "type")]
    _type: String,
    id: u64,
}

impl API {
    pub async fn get_activity_summaries(
        &self,
        page: usize,
        after: Option<OffsetDateTime>,
        before: Option<OffsetDateTime>,
    ) -> Result<Vec<ActivitySummary>, reqwest::Error> {
        let client = reqwest::Client::new();
        let url = format!(
            "https://www.strava.com/api/v3/athlete/activities?page={page}&per_page={PAGE_SIZE}{}{}",
            if let Some(before) = before {
                format!("&before={}", before.unix_timestamp())
            } else {
                String::from("")
            },
            if let Some(after) = after {
                format!("&after={}", after.unix_timestamp())
            } else {
                String::from("")
            },
        );
        Ok(client
            .get(url)
            .header("Authorization", format!("Bearer {}", self.token.token))
            .send()
            .await?
            .json()
            .await?)
    }
}
