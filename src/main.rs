use itertools::Itertools;
use native_tls::TlsConnector;
use postgres_native_tls::MakeTlsConnector;
use serde::{Deserialize, Serialize};
use std::{env, error::Error, fs};
use strava::{
    apis::{activities_api::get_logged_in_athlete_activities, configuration::Configuration},
    models::SummaryActivity,
};
use time::OffsetDateTime;

use strava_pacrun::auth::{get_new_code, Token};

#[derive(Serialize, Deserialize)]
struct Cache {
    token: Token,
    last_fetched: Option<OffsetDateTime>,
}

fn get_wkt_from_activity(activity: SummaryActivity) -> Option<String> {
    let points_list = polyline::decode_polyline(activity.map?.summary_polyline?.as_str(), 5)
        .unwrap()
        .points()
        .map(|p| format!("{} {}", p.x(), p.y()))
        .intersperse(String::from(","))
        .collect::<String>();
    if points_list.len() > 0 {
        Some(format!("LINESTRING({points_list})"))
    } else {
        None
    }
}

#[tokio::main]
async fn main() -> Result<(), Box<dyn Error>> {
    // If a cache file exists, load it
    let mut cache = if let Ok(cache_string) = fs::read_to_string("cache.toml") {
        let mut cache: Cache = toml::from_str(&cache_string)?;
        // If the token is expired, get a new one
        if cache.token.expires_at <= OffsetDateTime::now_utc() {
            cache.token = get_new_code().await?
        };
        cache
    } else {
        // If no cache file exists, initialize it with a new token
        Cache {
            token: get_new_code().await?,
            last_fetched: None,
        }
    };

    let strava_config = Configuration {
        base_path: "https://www.strava.com/api/v3".to_string(),
        client: reqwest::Client::new(),
        oauth_access_token: Some(cache.token.token.clone()),
        basic_auth: None,
        bearer_access_token: None,
        api_key: None,
        user_agent: None,
    };

    let connector = TlsConnector::new().unwrap();
    let connector = MakeTlsConnector::new(connector);
    let connection_string = env::var("PSQL_CONNECTION")
        .expect("No postgres connection string found. Set the PSQL_CONNECTION variable.");
    let (mut client, connection) = tokio_postgres::connect(&connection_string, connector).await?;
    tokio::spawn(async move {
        if let Err(e) = connection.await {
            eprintln!("connection error: {}", e);
        }
    });
    let transaction = client.transaction().await?;
    let insert_statement = transaction.prepare("INSERT INTO activities (strava_id, name, type, summary_map) VALUES ($1, $2, $3, ST_GeomFromText($4, 4326))").await?;

    let after = match cache.last_fetched {
        Some(t) => t.unix_timestamp() as i32,
        None => 0,
    };
    let mut results = true;
    let mut total_fetched = 0;
    let mut page = 1;
    while results {
        println!("Getting page {page}...");
        let activities = get_logged_in_athlete_activities(
            &strava_config,
            None,
            Some(after),
            Some(page),
            Some(30),
        )
        .await?;
        results = activities.len() > 0;
        page += 1;

        for activity in activities {
            let id = activity.id.unwrap();
            let name = activity.name.clone().unwrap_or("NO NAME".to_string());
            let _type = activity.r#type.unwrap().to_string();
            if let Some(wkt) = get_wkt_from_activity(activity) {
                if let Err(msg) = transaction
                    .execute(&insert_statement, &[&id, &name, &_type, &wkt])
                    .await
                {
                    eprintln!("Error inserting activity {}: {}", id, msg);
                    continue;
                }
                total_fetched += 1;
            }
        }
    }

    transaction.commit().await?;

    println!("Found {total_fetched} activities");

    cache.last_fetched = Some(OffsetDateTime::now_utc());
    // Write potential updates to the cache
    fs::write("cache.toml", toml::to_string(&cache)?)?;

    Ok(())
}
